import java.util.Objects

class Point(var x : Int, var y: Int) {
//    toString method
    override fun toString(): String{
        var test = x.toString();
        var test1 = y.toString();

        return "${test} ${test1}";
    }
//    equals method
    override fun equals(other: Any?): Boolean{
        if(other == null || other !is Point) return false
        return x == other.x && y == other.y
    }

    fun distance(objects: Point): Double{
        var distance = Math.sqrt(Math.pow((objects.x - this.x).toDouble(), 2.0) - Math.pow((objects.y - this.y).toDouble(), 2.0))
        return distance
    }

}